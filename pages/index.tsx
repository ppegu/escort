import React from "react";
import { GuestLayout } from "../components";
import Image from "next/image";
import { GIRLS } from "../config";
import { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faCheckCircle,
    faGrinHearts,
    faPhone,
} from "@fortawesome/free-solid-svg-icons";

export default function Home() {
    return (
        <GuestLayout>
            <div className="show_cage">
                {GIRLS(6).map((item: any, key: number) => {
                    return (
                        <div key={key} className="item">
                            <Image
                                loading="eager"
                                src={item.image}
                                width={700}
                                height={700}
                                alt={item.bio}
                                className="item"
                            />
                            <h2 className="item_title">{item.title}</h2>
                            <h3 className="item_location">{item.location}</h3>
                            <h4 className="item_bio">{item.bio}</h4>
                            <button type="button" className="item_button">
                                <h3>
                                    <FontAwesomeIcon
                                        style={{ color: "green" }}
                                        icon={faPhone}
                                    />{" "}
                                    Call Now
                                </h3>
                            </button>
                        </div>
                    );
                })}
            </div>

            <div className="intro">
                <div className="intro_box intro_box1">
                    <h2 className="intro_icon">
                        <FontAwesomeIcon icon={faGrinHearts} />
                    </h2>
                    <h2 className="intro_title">Do not Get Bored</h2>
                    <p className="intro_description">
                        Aenean auctor nisl vitae auctor faucibus. Pellentesque
                        imperdiet auctor eros, sit amet ornare mauris malesuada
                        in. Duis rutrum nisi tempus finibus luctus.
                    </p>
                </div>
                <div className="intro_box intro_box2">
                    <h2 className="intro_icon">
                        <FontAwesomeIcon icon={faGrinHearts} />
                    </h2>
                    <h2 className="intro_title">Do not Get Bored</h2>
                    <p className="intro_description">
                        Aenean auctor nisl vitae auctor faucibus. Pellentesque
                        imperdiet auctor eros, sit amet ornare mauris malesuada
                        in. Duis rutrum nisi tempus finibus luctus.
                    </p>
                </div>
                <div className="intro_box intro_box3">
                    <h2 className="intro_icon">
                        <FontAwesomeIcon icon={faGrinHearts} />
                    </h2>
                    <h2 className="intro_title">Do not Get Bored</h2>
                    <p className="intro_description">
                        Aenean auctor nisl vitae auctor faucibus. Pellentesque
                        imperdiet auctor eros, sit amet ornare mauris malesuada
                        in. Duis rutrum nisi tempus finibus luctus.
                    </p>
                </div>
            </div>

            <div className="benefites">
                <Image
                    src="/assets/images/girl4.jpg"
                    width={700}
                    height={700}
                    alt="girl4"
                    loading="eager"
                />
                <div className="benefites_box">
                    <div className="benefites_title">
                        <h2>ESCORT SERVICES</h2>
                    </div>
                    <ul>
                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>

                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>

                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>

                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>

                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>

                        <li>
                            <h3>
                                <FontAwesomeIcon icon={faCheckCircle} />{" "}
                                <a href="">BANGALORE ESCORT SERVICE</a>
                            </h3>
                        </li>
                    </ul>
                </div>
            </div>
        </GuestLayout>
    );
}
