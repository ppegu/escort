import { useRouter } from "next/dist/client/router";
import { GuestLayout } from "../components";
import Head from "next/head";

/*
 * Project : simple-escort
 * Version : 0.1.0
 * File Created : Monday, 19th July 2021 7:14:58 am
 * Author : Pran Pegu (pranpegu997@gmail.com)
 * -----
 * Last Modified: Monday, 19th July 2021 7:14:59 am
 * Modified By: Pran Pegu (pranpegu997@gmail.com>)
 */
const LocationBased = () => {
    const { location }: any = useRouter().query;
    return (
        <GuestLayout>
            <Head>
                <meta
                    httpEquiv="Content-Type"
                    content="text/html; charset=utf-8"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0"
                />
                <meta HTTP-EQUIV="CACHE-CONTROL" content="PUBLIC" />
                <title>
                    Assamese call girls in Guwahati looks beautiful meeting now
                </title>
                <meta
                    name="description"
                    content="Assam full of hot ans sexy available assamese call girls in Guwahati meeting for 
               booking contact us. We manage all things after booking call girls."
                />
                <meta property="og:locale" content="en_US" />
                <meta property="og:type" content="article" />
                <meta
                    property="og:title"
                    content="Assamese call girls in Guwahati looks beautiful meeting now"
                />
                <meta
                    property="og:description"
                    content="Assam full of hot ans sexy available assamese call girls in Guwahati 
               meeting for booking contact us. We manage all things after booking call girls."
                />
                <meta
                    property="og:url"
                    content="https://www.nityajoshi.com/assamese-call-girls-in-guwahati.html"
                />
                <meta property="og:site_name" content="Nitya Joshi" />
                <meta name="email" content="joshinitya55@gmail.com" />

                <meta name="Geography" content="guwahati, assam, india" />
                <meta name="city" content="guwahati" />
                <meta name="country" content="india" />
                <meta name="Robots" content="index, follow" />
                <meta name="googlebot" content="index,follow" />
                <meta name="Content-Language" content="EN" />
                <meta name="language" content="English" />
                <meta name="revisit-after" content="7 days" />
                <meta name="author" content="nitya joshi" />
                <meta name="Copyright" content="nityajoshi" />
                <meta name="distribution" content="Global" />
                <meta name="expires" content="365" />
                <meta name="Rating" content="General" />
            </Head>
        </GuestLayout>
    );
};

export default LocationBased;
