import { faBars } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
/*
 * Project : simple-escort
 * Version : 0.1.0
 * File Created : Sunday, 18th July 2021 4:47:06 pm
 * Author : Pran Pegu (pranpegu997@gmail.com)
 * -----
 * Last Modified: Sunday, 18th July 2021 4:47:06 pm
 * Modified By: Pran Pegu (pranpegu997@gmail.com>)
 */
export default function Navbar() {
    return (
        <div className="row justify-content-center">
            <div className="col-10 col-md-10">
                <nav className="col-12">
                    <div className="row justify-content-between align-items-center">
                        <div className="logo col-md-4 col-sm-10">
                            <h3 className="logo_text">Simple Escort</h3>
                        </div>
                        <div className="nav_menu col-md-8 d-none d-md-block">
                            <ul>
                                <li>
                                    <Link href="/">
                                        <a className="active">Home</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/">
                                        <a>Bangalore Escort</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/">
                                        <a>Mumbai Escort</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/">
                                        <a>Delhi Escort</a>
                                    </Link>
                                </li>
                                <li>
                                    <Link href="/">
                                        <a>More</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        <div className="nav_dropdown col-sm-2 d-sm-block d-none">
                            <FontAwesomeIcon icon={faBars} />
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    );
}
